# 2020-4xHEL_WFSST-PLF1

**Hinweis:** Das fertige Programm ist im Hauptordner vorhanden (*PLF1.exe*)

## Arbeitsschritte

- [ ] Erstelle ein neues Repo mit der Bezeichnung `20wfsst-PLF1_Nachname` auf GitLab
- [ ] Definiere `ruhxsi` als Maintainer des Projektes
- [ ] kopiere den Inhalt *des vorgegebenen* Repositories in dein eigenes Repo
- [ ] pushe dieses Repo auf GitLab
- [ ] Erstelle einen Branch `PLF1`und wechsle in diesen
- [ ] Öffne die vorgegebene Anwendung in Visual Studio
- [ ] mache 3 bis 4 commits im Intervall von ca. 30 Minuten und pushe diese hoch

### Graphische Oberfläche

- [ ] Passe das vorhandene GUI entsprechend dieser Abbildung an. Alles, was nicht angegeben ist, ist möglichst gut der Vorlage anzupassen (verbraucht nicht zu viel Zeit mit Details!)
  ![](MainWindow.png)

  - [ ] Es ist ein 2x2 Grid mit obigen Maßen zu erstellen
  - [ ] links oben die selbst klärenden Schaltflächen und die Eingabemöglichkeit; nutze sinnvolle Container
  - [ ] Links unten ist eine ListBox
  - [ ] Rechts werden beide Zeilen zusammengefasst; zentral ein Canvas mit 300x200, die Beschriftungen sind in der 2. Abb. zu entnehmen; die kleine horizontale Linie ist ein Rechteck

### Laufende Anwendung

  ![](MainWindowRunning.png)

- [ ] Wie ersichtlich, hängt die Höhe und Beschriftung des Balken vom gewählten Element der ListBox ab. Dies ist per **DataBinding** zu realisieren

### Erstelle eine Class Library (.net) Namens Model

- [ ] Erstelle in dieser Library die beiden Klassen entsprechend diesem Klassendigramm

  ![](ClassDiagramms.png)

- [ ] Das Serialisierformat kann von der Datei `\SnowStatistics_2.0\View\bin\Debug\Schneehöhen.csv` entnommen werden

- [ ] Das Format von `ToString()` ist aus der ListBox unten rechts ersichtlich

- [ ] Die restlichen Felder, Properties sowie Methoden sind aus dem Unterricht bekannt

### Folgende Ziele sind zu erreichen

- [ ] Richtiger Umgang mit Git
- [ ] GUI entsprechend der Vorlage
- [ ] Klassen `Snowheight ` sowie `SnowHeightList` entsprechend dem Klassendiagramm
- [ ] Daten aus dem CSV-File (relativer Pfad) lesen und in ListBox anzeigen
- [ ] Bei Auswählen eines ListBox Eintrags  diesen Wert im rechten Fenster graphisch darstellen (idealerweise per DataBinding)
- [ ] Abspeichern der Werte in einem CSV-File (relativer Pfad) 

### Wieder hochladen auf dein GitLab Repository

- [ ] Pushe das Programm wieder auf das GitLab Repository
- [ ] Merge den Branch PLF1 wieder mit dem Master Branch
- [ ] Kopiere allen Code (xaml und cs Code) via Cut 'n Paste in ein Word Dokument und erstelle daraus ein pdf und pushe dieses ebenfalls aufs Repo
